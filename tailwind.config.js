const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  darkMode: 'media',
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: ['./index.html', './src/**/*.vue', './src/**/*.ts'],
  },
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: '#154B62',
          dark: '#041014',
        },
        green: {
          DEFAULT: '#15E797',
        },
        blue: {
          DEFAULT: '#200681', // Color use when typing only 'blue'
          light: '#00ACED',
          dark: '#0c042e',
          transparent: '#4E2CCD20',
        },
        lightGray: {
          DEFAULT: '#F8F9FA',
        },
      },
    },
    fontFamily: {
      sans: ['Exo', 'sans-serif'],
      subtitle: ['Unica One', 'sans-serif'],
    },
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
  ],
}
