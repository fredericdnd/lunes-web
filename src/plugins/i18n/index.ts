import { Locales } from './locales'

import * as fr from '../../locales/fr.json'
import * as en from '../../locales/en.json'

export const messages = {
  [Locales.FR]: fr,
  [Locales.EN]: en,
}

export const defaultLocale = Locales.FR
