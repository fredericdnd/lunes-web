import meta from "./fr";

export const generateMeta = (route: string) => {
    const current_page = route.replace('/', '');

    if (current_page in meta) {
        return Object.assign({}, meta["default"], (meta as any)[current_page]);
    } else {
        return meta["default"];
    }
};
