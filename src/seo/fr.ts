export default {
    default: {
        title: "",
        description: "",
        meta: {
            property: {
                "og:title": "",
                "og:description": "",
                "twitter:title": "",
                "twitter:description": ""
            }
        }
    },
}
