import Home from '../views/Home.vue'
import NotFound from '../views/NotFound.vue'
import Legal from '../views/Legal.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
export const routes = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/legal',
    component: Legal,
  },
  {
    path: '/404',
    component: NotFound,
    meta: { title: 'NotFound' },
  },
  { path: '/:pathMatch(.*)*', redirect: '/404' },
]
