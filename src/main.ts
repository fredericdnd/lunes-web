import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './index.css'
import { createI18n } from 'vue-i18n'
import { messages, defaultLocale } from './plugins/i18n/index'
import VueLazyLoad from 'vue3-lazyload'

const app = createApp(App)

const i18n = createI18n({
  locale: defaultLocale,
  messages,
})

app.use(VueLazyLoad)

app.use(i18n)
app.use(router)
app.mount('#app')
